public class Main {

    public static void main(String[] args) {
        Human mother = new Human("Kate", "Smith", 1990);
        System.out.println(mother.toString());
        String [][] someTasks = new String[][] {
                {"Monday", "go to gym"},
                {"Friday", "relax"}
        };
        Human father = new Human("John", "Smith", 1989, 90,someTasks);
        System.out.println(father.toString());
        Family family = new Family(mother, father);
        System.out.println(family.toString());

        Human child = new Human("Mary", "Smith", 2022);
        Human child2 = new Human("Jane", "Smith", 2019);
        family.setChildren(new Human[]{child, child2});
        System.out.println("Add child:");
        System.out.println(family.toString());

        System.out.println("Deleting a child");
        boolean isDeleted = family.deleteChild(20);
        System.out.println("Deleting a child with wrong index: " + isDeleted);
        isDeleted = family.deleteChild(1);
        System.out.println("Deleting a child with right index: " + isDeleted);
        System.out.println(family.toString());

        System.out.println("Adding a child");
        Human child3 = new Human("Sam", "Smith", 2016);
        family.addChild(child3);
        System.out.println(family.toString());

        System.out.println("Adding a pet");
        Pet pet = new Pet("Cat", "Smurf");
        pet.setAge(3);
        System.out.println(pet);

        family.setPet(pet);
        child3.describePet();

    }

}
